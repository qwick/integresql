# integresql

## Installing
Right now this is not published to NPM but is usable via [Git URLs](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#git-urls-as-dependencies).

Add the git URL to your package.json:
```json
{
  "name": "my-cool-app",
  "devDependencies": {
    "bs-integresql": "git+https://gitlab.com/qwick/integresql#semver:^1.0.1"
  }
}

```

Then add it to bs-deps in bsconfig.json:
```json
{
  "bs-dependencies": ["bs-integresql"]
}
```

## Usage
See `src/Example.re`

## Publishing new versions
* update the version number in `./package.json` and `./bsconfig.json`
* run `./publish.sh`
* dependents can now install the new version!

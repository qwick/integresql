#! /bin/sh

# Naive "git package" workflow:
#
# origin/release is the same as origin/main but with build artifacts committed
#
# when a new version is published:
#  - undo the last "add .bs.js files" commit
#  - rebase onto main
#  - clean rebuild
#  - commit and force push
#
# This way origin/release is always 2 commits on top of origin/main.

echo -n "New version number (e.g. 'v1.0.0'): "
read version
git fetch --all
git switch release
git reset --hard origin/release
git reset --hard HEAD~1
git rebase origin/main
yarn clean
yarn build
git add -A
git commit -m 'chore: publish new build artifacts'
git tag $version
git push --force
git push --tags
git switch main

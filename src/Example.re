module Client =
  IntegreSQL.Client.Make(
    {
      let baseUrl = None;
    },
    IntegreSQL.Http.AxiosClient,
  );

// IO xtras
module IOx = {
  let flatTap = f => IO.flatMap(a => a |> f |> IO.map(const(a)));
};

let beforeAll =
  // RECOMMENDED:
  //   inspect your database and check if a template for this hash has already been created.
  //   if so, you can entirely skip this chunk of IO.
  Client.Template.setup(
    IO.suspendIO(() => {
      // Within this IO your goal should be to
      // recreate your production schema in the empty
      // template database, whose name is the hash you provided.
      //
      // This will typically mean running migrations and seeding the template with data

      let migrations = {|
        CREATE TABLE IF NOT EXISTS bar
          id INT PRIMARY KEY;

        CREATE TABLE IF NOT EXISTS foo
          id INT PRIMARY KEY,
          bar_id INT NOT NULL FOREIGN KEY REFERENCES bar(id);
      |};

      let _ = migrations; // run them somehow
      (IO.pure(): IO.t(unit, unit));
    }),
    "poop" |> IntegreSQL.Template.Hash.fromString,
  )
  |> IO.tap(const("[integresql_demo] template database ready") >> Js.log);

let afterAll = Client.Template.discard;

let beforeEach =
  IO.unsafeRunAsync(const(), beforeAll |> IO.flatMap(afterAll));

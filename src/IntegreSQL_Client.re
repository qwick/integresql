[@bs.deriving accessors]
type error('e) =
  | PostgresConnectionError
  | TemplateDoesNotExist(IntegreSQL_Template.Hash.t)
  | TemplateDiscarded(IntegreSQL_Template.Hash.t)
  | DecodeError(Decode.ParseError.failure)
  | JsExn(Js.Promise.error)
  | TemplateSetupError('e);

let%private stringifyError = e =>
  e |> Js.Json.stringifyAny |> Option.getOrElse("error not stringifiable :(");

let showError =
  fun
  | PostgresConnectionError => "PostgresConnectionError"
  | TemplateDoesNotExist(h) =>
    ["TemplateDoesNotExist(\"", h |> IntegreSQL_Template.Hash.toString, "\")"]
    |> List.String.join
  | TemplateDiscarded(h) =>
    ["TemplateDiscarded(\"", h |> IntegreSQL_Template.Hash.toString, "\")"]
    |> List.String.join
  | DecodeError(e) =>
    ["DecodeError(\"", e |> Decode.ParseError.failureToDebugString, "\")"]
    |> List.String.join
  | JsExn(e) =>
    [
      "JsExn(\"",
      e |> stringifyError,
      "\")",
      "\n/* ",
      e |> stringifyError,
      " */",
    ]
    |> List.String.join
  | TemplateSetupError(e) =>
    ["TemplateSetupError(\"", e |> stringifyError, "\")", "\n/* "]
    |> List.String.join;

let joinPathSegments = List.String.joinWith("/");

module type Client = {
  module Template: {
    type initializeResult =
      | AlreadyInitialized(IntegreSQL_Template.Hash.t)
      | ReadyForSecretSauce(IntegreSQL_Template.Hash.t);

    let setup:
      (IO.t(unit, 'e), IntegreSQL_Template.Hash.t) =>
      IO.t(IntegreSQL_Template.Hash.t, error('e));

    let initialize:
      IntegreSQL_Template.Hash.t => IO.t(initializeResult, error('e));

    let finalize:
      IntegreSQL_Template.Hash.t =>
      IO.t(IntegreSQL_Template.Hash.t, error('e));

    let discard: IntegreSQL_Template.Hash.t => IO.t(unit, error('e));
  };

  module Database: {
    let spinUp:
      IntegreSQL_Template.Hash.t => IO.t(IntegreSQL_Database.t, error('e));
    let teardown:
      (IntegreSQL_Template.Hash.t, IntegreSQL_Database.Id.t) =>
      IO.t(unit, error('e));
  };
};

module Make =
       (C: {let baseUrl: option(string);}, Http: IntegreSQL_Http.Client) => {
  let baseUrl = C.baseUrl |> Option.getOrElse("http://0.0.0.0:9898/api/v1");

  let flatMapStatus = (s, ~matched, ~otherwise) =>
    IO.flatMap(({status} as resp: Http.response(_)) =>
      status == s ? matched(resp) : otherwise(resp)
    );

  let rejectStatus = (s, error) =>
    flatMapStatus(s, ~matched=error |> IO.throw |> const, ~otherwise=IO.pure);

  let handleError = io =>
    io
    |> IO.mapError(e => jsExn(e))
    |> rejectStatus(503, postgresConnectionError);

  module Template = {
    type initializeResult =
      | AlreadyInitialized(IntegreSQL_Template.Hash.t)
      | ReadyForSecretSauce(IntegreSQL_Template.Hash.t);

    let baseUrl = [baseUrl, "templates"] |> joinPathSegments;
    let hashUrl = hash =>
      [baseUrl, hash |> IntegreSQL_Template.Hash.toString] |> joinPathSegments;

    let initialize:
      'e.
      IntegreSQL_Template.Hash.t => IO.t(initializeResult, error('e))
     =
      hash =>
        Http.post(
          ~url=baseUrl,
          ~body=
            [
              (
                "hash",
                hash |> IntegreSQL_Template.Hash.toString |> Js.Json.string,
              ),
            ]
            |> Js.Dict.fromList
            |> Js.Json.object_,
          (),
        )
        |> handleError
        |> flatMapStatus(
             423,
             ~matched=AlreadyInitialized(hash) |> IO.pure |> const,
             ~otherwise=ReadyForSecretSauce(hash) |> IO.pure |> const,
           );

    let finalize = hash =>
      Http.put(~url=hash |> hashUrl, ~body=Js.Json.null, ())
      |> handleError
      |> IO.map(hash |> const);

    let discard = hash =>
      Http.delete(~url=hash |> hashUrl, ()) |> handleError |> IO.map(const());

    let setup = (work, hash) =>
      hash
      |> initialize
      // if user-provided work errors, discard the partially-worked-on template, and continue with the error
      // _(note that if the work fails and then discard fails, the discard error will swallow the work error)_
      |> IO.flatMap(
           work
           |> IO.catchError(e =>
                hash
                |> discard
                |> IO.flatMap(e |> templateSetupError |> IO.throw |> const)
              )
           |> const,
         )
      |> IO.flatMap(hash |> finalize |> const);
  };

  module Database = {
    let baseUrl = tpl =>
      [
        baseUrl,
        "templates",
        tpl |> IntegreSQL_Template.Hash.toString,
        "tests",
      ]
      |> joinPathSegments;

    let testUrl = (tpl, id) =>
      [tpl |> baseUrl, id |> IntegreSQL_Database.Id.toString]
      |> joinPathSegments;

    let spinUp = tpl =>
      Http.get(~url=tpl |> baseUrl, ())
      |> handleError
      |> rejectStatus(404, tpl |> templateDoesNotExist)
      |> rejectStatus(410, tpl |> templateDiscarded)
      |> IO.flatMap(
           Http.responseJson
           >> IntegreSQL_Database.fromJson
           >> Result.mapError(decodeError)
           >> IO.fromResult,
         );

    let teardown = (tpl, id) =>
      Http.delete(~url=id |> testUrl(tpl), ())
      |> handleError
      |> IO.map(const());
  };
};

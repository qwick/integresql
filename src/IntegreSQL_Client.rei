type error('e) =
  | PostgresConnectionError
  | TemplateDoesNotExist(IntegreSQL_Template.Hash.t)
  | TemplateDiscarded(IntegreSQL_Template.Hash.t)
  | DecodeError(Decode.ParseError.failure)
  | JsExn(Js.Promise.error)
  | TemplateSetupError('e);

let showError: error('e) => string;

module type Client = {
  module Template: {
    type initializeResult =
      | AlreadyInitialized(IntegreSQL_Template.Hash.t)
      | ReadyForSecretSauce(IntegreSQL_Template.Hash.t);

    let setup: (IO.t(unit, 'e), IntegreSQL_Template.Hash.t) => IO.t(IntegreSQL_Template.Hash.t, error('e));

    let initialize:
      IntegreSQL_Template.Hash.t => IO.t(initializeResult, error('e));

    let finalize:
      IntegreSQL_Template.Hash.t =>
      IO.t(IntegreSQL_Template.Hash.t, error('e));

    let discard: IntegreSQL_Template.Hash.t => IO.t(unit, error('e));
  };
  module Database: {
    let spinUp:
      IntegreSQL_Template.Hash.t => IO.t(IntegreSQL_Database.t, error('e));
    let teardown:
      (IntegreSQL_Template.Hash.t, IntegreSQL_Database.Id.t) =>
      IO.t(unit, error('e));
  };
};

module Make:
  (C: {let baseUrl: option(string);}, Http: IntegreSQL_Http.Client) => Client;

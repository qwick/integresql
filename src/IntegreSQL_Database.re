module Id =
  IntegreSQL_Newtype.Int({});

module Port =
  IntegreSQL_Newtype.Int({});

module Host =
  IntegreSQL_Newtype.String({});

module Username =
  IntegreSQL_Newtype.String({});

module Password =
  IntegreSQL_Newtype.String({});

module Database =
  IntegreSQL_Newtype.String({});

module Config = {
  [@bs.deriving accessors]
  type t = {
    host: Host.t,
    port: Port.t,
    username: Username.t,
    password: Password.t,
    database: Database.t,
    additionalParams: option(String.Map.t(string)),
  };

  let fromJson = json =>
    IntegreSQL_Decode.(
      (
        (host, port, username, password, database, additionalParams) => {
          host,
          port,
          username,
          password,
          database,
          additionalParams,
        }
      )
      <$> field("host", Host.fromJson, json)
      <*> field("port", Port.fromJson, json)
      <*> field("username", Username.fromJson, json)
      <*> field("password", Password.fromJson, json)
      <*> field("database", Database.fromJson, json)
      <*> optionalField(
            "additionalParams",
            stringMap(string)
            |> map(Belt_MapString.toArray >> String.Map.fromArray),
            json,
          )
    );
};

[@bs.deriving accessors]
type t = {
  id: Id.t,
  config: Config.t,
  templateHash: string,
};

type raw = {
  id: Id.t,
  database,
}
and database = {
  config: Config.t,
  templateHash: string,
};

let rawToT = ({id, database: {config, templateHash}}: raw) => {
  id,
  config,
  templateHash,
};

let rawDbFromJson = json =>
  IntegreSQL_Decode.(
    ((config, templateHash) => {config, templateHash})
    <$> field("config", Config.fromJson, json)
    <*> field("templateHash", string, json)
  );

let rawFromJson = json =>
  IntegreSQL_Decode.(
    ((id, database) => {id, database})
    <$> field("id", Id.fromJson, json)
    <*> field("database", rawDbFromJson, json)
  );

let fromJson = rawFromJson >> Result.map(rawToT);

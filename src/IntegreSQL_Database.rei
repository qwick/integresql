module Id: IntegreSQL_Newtype.INT;

module Port: IntegreSQL_Newtype.INT;

module Host: IntegreSQL_Newtype.STRING;

module Username: IntegreSQL_Newtype.STRING;

module Password: IntegreSQL_Newtype.STRING;

module Database: IntegreSQL_Newtype.STRING;

module Config: {
  type t;

  let host: t => Host.t;
  let port: t => Port.t;
  let username: t => Username.t;
  let password: t => Password.t;
  let database: t => Database.t;
  let additionalParams: t => option(String.Map.t(string));

  let fromJson: Js.Json.t => Result.t(t, IntegreSQL_Decode.failure);
};

type t;
let id: t => Id.t;
let config: t => Config.t;
let templateHash: t => string;

let fromJson: Js.Json.t => Result.t(t, IntegreSQL_Decode.failure);

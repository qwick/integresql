module R =
  Result.WithError({
    type t = Decode.ParseError.failure;
  });

include R.Infix;
include Decode.AsResult.OfParseError;
type failure = Decode.ParseError.failure;

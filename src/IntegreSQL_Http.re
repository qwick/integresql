module type Client = {
  type config;
  type response('h) = {
    config,
    data: Js.Json.t,
    headers: Js.t('h),
    status: int,
  };

  let responseJson: response(_) => Js.Json.t;

  let emptyConfig: config;

  let get:
    (~url: string, ~config: config=?, unit) =>
    IO.t(response('h), Js.Promise.error);
  let post:
    (~url: string, ~body: Js.Json.t, ~config: config=?, unit) =>
    IO.t(response('h), Js.Promise.error);
  let put:
    (~url: string, ~body: Js.Json.t, ~config: config=?, unit) =>
    IO.t(response('h), Js.Promise.error);
  let delete:
    (~url: string, ~config: config=?, unit) =>
    IO.t(response('h), Js.Promise.error);
};

module AxiosClient = {
  // uncomment to configure axios to dump debug logs
  // [%raw "require('axios-debug-log/enable')"];

  type config = Axios_types.config;
  type response('h) = {
    config,
    data: Js.Json.t,
    headers: Js.t('h),
    status: int,
  };

  let responseJson = ({data}) => data;

  let jsToJson: Js.t('a) => Js.Json.t = Relude.Unsafe.coerce;
  let jsonToJs: Js.Json.t => Js.t('a) = Relude.Unsafe.coerce;

  let scrubResponse: Axios_types.response('b, 'h) => response('h) =
    obj => {
      config: obj##config,
      status: obj##status,
      headers: obj##headers,
      data: obj##data |> jsToJson,
    };

  let emptyConfig =
    Axios.makeConfig(
      ~responseType="json",
      // dear axios authors, rejecting promises on non-OK
      // responses is a very stupid default 🙃
      ~validateStatus=const(true),
      (),
    );

  let toIO = work =>
    work |> Relude_Js_Promise.toIOLazy |> IO.map(scrubResponse);

  let get = (~url, ~config=?, ()) =>
    toIO(() => Axios.getc(url, config |> Option.getOrElse(emptyConfig)));

  let post = (~url, ~body, ~config=?, ()) =>
    toIO(() =>
      Axios.postDatac(
        url,
        body |> jsonToJs,
        config |> Option.getOrElse(emptyConfig),
      )
    );

  let put = (~url, ~body, ~config=?, ()) =>
    toIO(() =>
      Axios.putDatac(
        url,
        body |> jsonToJs,
        config |> Option.getOrElse(emptyConfig),
      )
    );

  let delete = (~url, ~config=?, ()) =>
    toIO(() => Axios.deletec(url, config |> Option.getOrElse(emptyConfig)));
};

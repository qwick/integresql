module type Client = {
  type config;
  type response('h) = {config, data: Js.Json.t, headers: Js.t('h), status: int};
  let responseJson: response(_) => Js.Json.t;

  let emptyConfig: config;

  let get: (~url:string, ~config: config=?, unit) => IO.t(response('h), Js.Promise.error);
  let post: (~url:string, ~body: Js.Json.t, ~config: config=?, unit) => IO.t(response('h), Js.Promise.error);
  let put: (~url:string, ~body: Js.Json.t, ~config : config=?, unit) => IO.t(response('h), Js.Promise.error);
  let delete: (~url:string, ~config: config=?, unit) => IO.t(response('h), Js.Promise.error);
};

module AxiosClient: {
  type config = Axios_types.config;

  include Client with type config := config;
};

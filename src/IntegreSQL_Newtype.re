module type STRING = {
  type t;
  let fromString: string => t;
  let toString: t => string;
  let fromJson: Js.Json.t => Result.t(t, Decode.ParseError.failure);
};

module type INT = {
  type t;
  let fromInt: int => t;
  let toInt: t => int;
  let toString: t => string;
  let fromJson: Js.Json.t => Result.t(t, Decode.ParseError.failure);
};

module String = (T: {}) : STRING => {
  type t = string;
  let fromString = id;
  let toString = id;
  let fromJson = Decode.AsResult.OfParseError.string;
};

module Int = (T: {}) : INT => {
  type t = int;
  let fromInt = id;
  let toInt = id;
  let toString = Int.toString;
  let fromJson = Decode.AsResult.OfParseError.intFromNumber;
};

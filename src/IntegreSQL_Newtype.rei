module type STRING = {
  type t;
  let fromString: string => t;
  let toString: t => string;
  let fromJson: Js.Json.t => Result.t(t, Decode.ParseError.failure);
};

module type INT = {
  type t;
  let fromInt: int => t;
  let toInt: t => int;
  let toString: t => string;
  let fromJson: Js.Json.t => Result.t(t, Decode.ParseError.failure);
};

module String: (T: {}) => STRING;

module Int: (T: {}) => INT;
